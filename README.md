### Fix locale issue ###
* $ export LC_ALL="en_US.UTF-8"
* $ sudo echo 'LC_ALL="en_US.UTF-8"' >> /etc/default/locale
* $ sudo locale-gen "en_US.UTF-8"
* $ sudo update-locale

### Add new user ###

* $ adduser sammy
* $ usermod -aG sudo sammy

### Copy the Public Key ssh ###

* $ ssh-copy-id user@123.45.56.78

### Update Kernel on Ubuntu ###

* $ sudo apt-get update
* $ sudo apt-get install linux-virtual
* $ sudo reboot